import json
import os

from flask import Flask, request

import semgrep
from semgrep import semgrep_main
from semgrep.state import get_state
from semgrep.constants import OutputFormat
from semgrep.output import OutputHandler, OutputSettings

RULE_VERSION = '2.1.2'

app = Flask(__name__)

def load_rules():
    paths = []
    for dirpath, dirnames, filenames in os.walk('./dist'):
        for filename in filenames:
            if filename.endswith('.yml'):
                paths = paths + [dirpath + '/' + filename]
    return paths

@app.route('/scan', methods=['POST'])
def scan():
    """Scan route."""

    file = request.files['file']
    temp_path = f'/tmp/{file.filename}'
    file.save(temp_path)

    state = get_state()
    output_settings = OutputSettings(output_format=OutputFormat.JSON, error_on_findings=False)
    output_handler = OutputHandler(output_settings) 

    # configs = ["./tests/assets/rule-usleep.yml"]
    # targets = ["./tests/rule-usleep.c"]
    # configs = load_rules()
    configs = ['./dist/eslint.yml', './dist/flawfinder.yml', './dist/gosec.yml']
    targets = [temp_path]

    (
        filtered_matches_by_rule,
        _,
        _,
        _,
        _,
        _,
        _,
        _,
        _,
        _,
        _,
        _,
    ) = semgrep_main.main(
        output_handler=output_handler,
        target=targets,
        jobs=1,
        pattern=None,
        lang=None,
        configs=configs,
        timeout=5,
        timeout_threshold=3
    )
    output_handler.rule_matches = [
        m for ms in filtered_matches_by_rule.values() for m in ms
    ]

    return json.loads(output_handler._build_output())

@app.route("/")
def root():
    """Version route."""
    return f"Semgrep Engine v{semgrep.__VERSION__}, Rule v{RULE_VERSION}"


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))
