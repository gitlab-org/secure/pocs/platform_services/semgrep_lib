rules:
	curl https://gitlab.com/gitlab-org/security-products/sast-rules/-/package_files/118852067/download -o sast-rules-v2.1.2.zip
	unzip sast-rules-v2.1.2.zip

deploy:
	gcloud run deploy